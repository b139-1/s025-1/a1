
// Count total fruits on sale
db.fruits.aggregate(
	{ $match: {onSale: true}}, {$count: "fruitsOnSale"}
);

// Count total number of fruits with stock more than 20
db.fruits.aggregate(
	{ $match: {stock: {$gte: 20}}}, {$count: "enoughStock"}
);

// Get the acerage price of fruits onSale per supplier
db.fruits.aggregate(
	[
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}}
	]
);

// Get the highest price of a fruit per supplier
db.fruits.aggregate(
	[
		{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
	]
);

// Get the lowest price of a fruit per supplier
db.fruits.aggregate(
	[
		{$group: {_id: "$supplier_id", maxPrice: {$min: "$price"}}}
	]
);